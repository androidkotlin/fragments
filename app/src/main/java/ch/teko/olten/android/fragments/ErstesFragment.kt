package ch.teko.olten.android.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView


/**
 * A simple [Fragment] subclass.
 *
 */
class ErstesFragment : Fragment() {

    val fragment2 = ZweitesFragment()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_erstes, container, false)

        val laengeBtn = view?.findViewById<Button>(R.id.laengeBtn)
        laengeBtn?.setOnClickListener {
            val editText = view.findViewById<EditText>(R.id.editText)
            val laengeTxt = view.findViewById<TextView>(R.id.laengeTxt)
            laengeTxt.text = editText.text.length.toString()
        }

        val fragment2Btn = view?.findViewById<Button>(R.id.startFragment)
        fragment2Btn?.setOnClickListener {

            val bundle = Bundle()
            bundle.putString("key", "122345")
            fragment2.arguments = bundle

            val transaction = this.fragmentManager?.beginTransaction()

            transaction?.let {
                it.replace(R.id.frame, fragment2)
                        .addToBackStack(null)
                        .commit()
            }
        }
        return view
    }


}
