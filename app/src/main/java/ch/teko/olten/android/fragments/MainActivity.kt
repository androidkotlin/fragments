package ch.teko.olten.android.fragments

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    val erstesFragment = ErstesFragment()
    val zweitesFragment = ZweitesFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragment1Btn.setOnClickListener {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame, erstesFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        fragment2Btn.setOnClickListener {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame, zweitesFragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }

    }
}
